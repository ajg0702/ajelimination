package us.ajg0702.elimination;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class Messages {
	
	File file;
	YamlConfiguration msgs;
	
	public String get(String key) {
		String raw;
		if(msgs.isSet(key)) {
			raw = msgs.getString(key);
		} else {
			raw = "&4| &cCould not find the message '" + key + "'!";
		}
		raw = ChatColor.translateAlternateColorCodes('&', raw);
		return raw;
	}
	
	public void reload() {
		msgs = YamlConfiguration.loadConfiguration(file);
	}
	
	static Messages INSTANCE;
	public static Messages getInstance() {
		return INSTANCE;
	}
	
	
	public Messages(JavaPlugin plugin) {
		file = new File(plugin.getDataFolder(), "messages.yml");
		INSTANCE = this;
		msgs = YamlConfiguration.loadConfiguration(file);
		Map<String, String> msgDefaults = new HashMap<String, String>();
		
		msgDefaults.put("noperm", "&cYou do not have permission to do this!");
		
		msgDefaults.put("not-from-console", "&cThis command must be executed in-game!");
		
		msgDefaults.put("revive.not-found", "&cThat player could not be found.");
		msgDefaults.put("revive.success", "&aYou've revived {PLAYER}.");
		msgDefaults.put("revive.success-all", "&aYou've revived everyone.");
		msgDefaults.put("tpalive", "&aTeleported all alive players to you.");
		
		msgDefaults.put("reloaded", "&aReloaded the config and messages!");
		
		msgDefaults.put("setpos.revive", "&aSet the revive point!");
		
		msgDefaults.put("placeholders.status.alive", "alive");
		msgDefaults.put("placeholders.status.dead", "dead");
		
		msgDefaults.put("status-update.eliminated", "&4✘&c You've been eliminated from the event");
		msgDefaults.put("status-update.revived", "&aYou've been revived.");
		
		//msgDefaults.put("", "");
		
		for(String key : msgDefaults.keySet()) {
			if(!msgs.isSet(key)) {
				msgs.set(key, msgDefaults.get(key));
			}
		}
		
		Map<String, String> mv = new HashMap<String, String>();
		//mv.put("before.path", "after.path");
		
		for(String key : mv.keySet()) {
			if(msgs.isSet(key)) {
				msgs.set(mv.get(key), msgs.getString(key));
				msgs.set(key, null);
			}
		}
		
		
		
		msgs.options().header("\n\nThis is the messsages file.\nYou can change any messages that are in this file\n\nIf you want to reset a message back to the default,\ndelete the entire line the message is on and restart the server.\n\t\n\t");
		try {
			msgs.save(file);
		} catch (IOException e) {
			Bukkit.getLogger().warning("[ajAntiXray] Could not save messages file!");
		}
	}
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
}
