package us.ajg0702.elimination;

import org.bukkit.entity.Player;

import me.tigerhix.lib.scoreboard.type.Scoreboard;

public class EventPlayer {
	Player ply;
	boolean alive;
	public EventPlayer(Player ply, boolean alive) {
		this.ply = ply;
		this.alive = alive;
	}
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	public boolean isAlive() {
		return alive;
	}
	
	Scoreboard sb;
}
