package us.ajg0702.elimination.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.ajg0702.elimination.Main;
import us.ajg0702.elimination.Messages;
import us.ajg0702.elimination.PositionManager;

public class MainCommand implements CommandExecutor {
	
	Main plugin;
	Messages msgs;
	public MainCommand(Main pl) {
		this.plugin = pl;
		this.msgs = Messages.getInstance();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length < 1) {
			sender.sendMessage(getHelpMessage(label));
			return true;
		}
		if(args[0].equalsIgnoreCase("reload")) {
			if(!sender.hasPermission("ajelimination.reload")) {
				sender.sendMessage(Messages.getInstance().get("noperm"));
				return true;
			}
			plugin.getAConfig().reload();
			Messages.getInstance().reload();
			sender.sendMessage(Messages.getInstance().get("reloaded"));
			return true;
		}
		if(args[0].equalsIgnoreCase("setrevive")) {
			if(!sender.hasPermission("ajelimination.setrevive")) {
				sender.sendMessage(Messages.getInstance().get("noperm"));
				return true;
			}
			if(!(sender instanceof Player)) {
				sender.sendMessage(msgs.get("not-from-console"));
				return true;
			}
			PositionManager.getInstance().setPosition("revive", ((Player) sender).getLocation());
			sender.sendMessage(msgs.get("setpos.revive"));
			return true;
		}
		sender.sendMessage(getHelpMessage(label));
		return true;
	}
	
	private String getHelpMessage(String label) {
		return msgs.color("&6ajElimination &ev"+plugin.getDescription().getVersion()+"&6 by &eajgeiss0702\n"
				+ " &eCommands:\n"
				+ "  &e/{l} &7- &6Shows this help message\n"
				+ "  &e/{l} reload &7- &6Reloads the config and the messages files.\n"
				+ "  &e/{l} setrevive &7- &6Sets the point revived players will be teleported to").replaceAll("\\{l\\}", label);
	}

}
