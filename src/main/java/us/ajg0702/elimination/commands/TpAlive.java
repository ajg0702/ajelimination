package us.ajg0702.elimination.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.ajg0702.elimination.EventPlayer;
import us.ajg0702.elimination.Manager;
import us.ajg0702.elimination.Messages;

public class TpAlive implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Messages.getInstance().get("not-from-console"));
			return true;
		}
		if(!sender.hasPermission("ajelimination.tpalive")) {
			sender.sendMessage(Messages.getInstance().get("noperm"));
			return true;
		}
		for(Player ply : Bukkit.getOnlinePlayers()) {
			EventPlayer p = Manager.getInstance().getEventPlayer(ply);
			if(p.isAlive()) {
				ply.teleport(((Player)sender));
			}
		}
		return true;
	}

}
