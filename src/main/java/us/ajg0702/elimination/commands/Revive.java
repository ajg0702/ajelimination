package us.ajg0702.elimination.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.ajg0702.elimination.Manager;
import us.ajg0702.elimination.Messages;
import us.ajg0702.elimination.PositionManager;

public class Revive implements CommandExecutor {
	

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length < 1) return false;
		if(!sender.hasPermission("ajelimination.revive")) {
			sender.sendMessage(Messages.getInstance().get("noperm"));
			return true;
		}
		if(args[0].equalsIgnoreCase("all")) {
			for(Player ply : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().revive(ply);
				if(PositionManager.getInstance().getPosition("revive") == null && sender instanceof Player) {
					ply.teleport(((Player) sender));
				}
			}
			sender.sendMessage(Messages.getInstance().get("revive.success-all"));
			return true;
		}
		Player rev = Bukkit.getPlayer(args[0]);
		if(rev == null) {
			sender.sendMessage(Messages.getInstance().get("revive.not-found"));
			return true;
		}
		Manager.getInstance().revive(rev);
		if(PositionManager.getInstance().getPosition("revive") == null && sender instanceof Player) {
			rev.teleport(((Player) sender));
		}
		sender.sendMessage(Messages.getInstance().get("revive.success").replaceAll("\\{PLAYER\\}", rev.getName()));
		return true;
	}

}
