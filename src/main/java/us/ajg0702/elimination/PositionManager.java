package us.ajg0702.elimination;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

public class PositionManager {
	static PositionManager INSTANCE = null;
	public static PositionManager getInstance() {
		return INSTANCE;
	}
	public static PositionManager getIntance(Main pl) {
		if(INSTANCE == null) {
			INSTANCE = new PositionManager(pl);
		}
		return INSTANCE;
	}
	
	
	Main plugin;
	
	File file;
	YamlConfiguration yml;
	
	private PositionManager(Main pl) {
		this.plugin = pl;
		
		file = new File(pl.getDataFolder(), "positions.yml");
		reload();
	}
	
	public void reload() {
		yml = YamlConfiguration.loadConfiguration(file);
	}
	
	public void setPosition(String key, Location loc) {
		yml.set(key, locToString(loc));
		try {
			yml.save(file);
		} catch (IOException e) {
			plugin.getLogger().warning("Unable to save location '"+key+"'!");
			e.printStackTrace();
		}
	} 
	public Location getPosition(String key) {
		String rawpos = yml.getString(key);
		if(rawpos == null) return null;
		
		return stringToLoc(rawpos);
	}
	
	
	public static String locToString(Location loc) {
		String ls = "";
		ls += loc.getWorld().getName()+",";
		ls += loc.getX()+",";
		ls += loc.getY()+",";
		ls += loc.getZ()+",";
		ls += loc.getYaw()+",";
		ls += loc.getPitch()+"";
		return ls;
	}
	public static Location stringToLoc(String ls) {
		String[] parts = ls.split(",");
		return new Location(
				Bukkit.getWorld(parts[0]),
				Double.valueOf(parts[1]),
				Double.valueOf(parts[2]),
				Double.valueOf(parts[3]),
				Float.valueOf(parts[4]),
				Float.valueOf(parts[5])
				);
	}
}
