package us.ajg0702.elimination;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.clip.placeholderapi.PlaceholderAPI;
import me.tigerhix.lib.scoreboard.ScoreboardLib;
import me.tigerhix.lib.scoreboard.common.EntryBuilder;
import me.tigerhix.lib.scoreboard.type.Entry;
import me.tigerhix.lib.scoreboard.type.ScoreboardHandler;
import us.ajg0702.elimination.utils.Config;
import us.ajg0702.elimination.utils.VersionSupport;

public class Manager {
	
	static Manager INSTANCE = null;
	public static Manager getInstance() {
		return INSTANCE;
	}
	public static Manager getInstance(Main pl) {
		if(INSTANCE == null) {
			INSTANCE = new Manager(pl);
		}
		return INSTANCE;
	}
	
	Main plugin;
	Messages msgs;
	Config config;
	private Manager(Main pl) {
		plugin = pl;
		this.msgs = pl.msgs;
		this.config = pl.config;
		
		sb = Bukkit.getScoreboardManager().getMainScoreboard();
		
		aliveTeam = sb.getTeam("ajelim-alive");
		if(aliveTeam == null) {
			aliveTeam = sb.registerNewTeam("ajelim-alive");
		}
		aliveTeam.setPrefix(msgs.color("&a"));
		if(VersionSupport.getMinorVersion() >= 12) {
			aliveTeam.setColor(ChatColor.GREEN);
		}
		
		deadTeam = sb.getTeam("ajelim-dead");
		if(deadTeam == null) {
			deadTeam = sb.registerNewTeam("ajelim-dead");
		}
		deadTeam.setPrefix(msgs.color("&c"));
		if(VersionSupport.getMinorVersion() >= 12) {
			deadTeam.setColor(ChatColor.RED);
		}
	}
	
	Scoreboard sb;
	Team aliveTeam;
	Team deadTeam;
	
	List<EventPlayer> plys = new ArrayList<>();
	
	
	public void addPlayer(Player ply) {
		if(getEventPlayer(ply) != null) return;
		plys.add(new EventPlayer(ply, false));
		deadTeam.addEntry(ply.getName());
		addScoreboard(ply);
	}
	public void removePlayer(Player ply) {
		EventPlayer p = getEventPlayer(ply);
		if(p == null) return;
		plys.remove(p);
		aliveTeam.removeEntry(ply.getName());
		deadTeam.removeEntry(ply.getName());
	}
	
	
	public EventPlayer getEventPlayer(Player ply) {
		for(EventPlayer p : plys) {
			if(p.ply.equals(ply)) return p;
		}
		return null;
	}
	
	public int getRemaining() {
		int count = 0;
		for(EventPlayer p : plys) {
			if(p.isAlive()) count++;
		}
		return count;
	}
	
	
	public void eliminate(Player ply) {
		EventPlayer p = getEventPlayer(ply);
		if(!p.alive) return;
		p.setAlive(false);
		aliveTeam.removeEntry(ply.getName());
		deadTeam.addEntry(ply.getName());
		
		ply.sendMessage(msgs.get("status-update.eliminated"));
	}
	public void revive(Player ply) {
		EventPlayer p = getEventPlayer(ply);
		if(p.alive) return;
		p.setAlive(true);
		deadTeam.removeEntry(ply.getName());
		aliveTeam.addEntry(ply.getName());
		
		Location revPos = PositionManager.getInstance().getPosition("revive");
		if(revPos != null) {
			ply.teleport(revPos);
		}
		
		ply.sendMessage(msgs.get("status-update.revived"));
	}
	
	
	public void addScoreboard(Player ply) {
		if(!config.getBoolean("enable-scoreboard")) return;
		EventPlayer p = getEventPlayer(ply);
		p.sb = ScoreboardLib.createScoreboard(ply)
				.setHandler(new ScoreboardHandler() {

					@Override
					public List<Entry> getEntries(Player ply) {
						EntryBuilder b = new EntryBuilder();
						List<String> scoreboardFormat = config.getStringList("scoreboard-format");
						scoreboardFormat.remove(0);
						for(String s : scoreboardFormat) {
							//ply.sendMessage(msgs.color(s));
							String line = s.replaceAll("\\{REMAINING\\}", getRemaining()+"");
							if(plugin.papi) {
								line = PlaceholderAPI.setPlaceholders(p.ply, line);
							}
							b.next(line);
						}
						List<Entry> es = b.build();
						/*for(Entry e : es) {
							ply.sendMessage(e.getName()+"   "+e.getPosition());
						}*/
						return es;
					}

					@Override
					public String getTitle(Player ply) {
						return config.getStringList("scoreboard-format").get(0);
					}
					
				}).setUpdateInterval(20);
		p.sb.activate();
	}
}
