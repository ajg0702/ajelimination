package us.ajg0702.elimination;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.tigerhix.lib.scoreboard.ScoreboardLib;
import us.ajg0702.elimination.commands.MainCommand;
import us.ajg0702.elimination.commands.Revive;
import us.ajg0702.elimination.commands.TpAlive;
import us.ajg0702.elimination.commands.TpDead;
import us.ajg0702.elimination.utils.Config;

public class Main extends JavaPlugin implements Listener {
	
	Manager man;
	Messages msgs;
	Config config;
	
	boolean papi = false;
	
	Placeholders placeholders;
	
	@Override
	public void onEnable() {
		config = new Config(this);
		msgs = new Messages(this);
		man = Manager.getInstance(this);
		
		PositionManager.getIntance(this);
		
		
		papi = Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null;
		if(papi) {
			placeholders = new Placeholders(this);
			placeholders.register();
		}
		
		
		Bukkit.getPluginManager().registerEvents(this, this);
		
		this.getCommand("revive").setExecutor(new Revive());
		this.getCommand("ajelimination").setExecutor(new MainCommand(this));
		this.getCommand("tpalive").setExecutor(new TpAlive());
		this.getCommand("tpdead").setExecutor(new TpDead());
		
		ScoreboardLib.setPluginInstance(this);
		
		this.getLogger().info("v"+this.getDescription().getVersion()+" by ajgeiss0702 enabled!");
	}
	
	public Config getAConfig() {
		return config;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		man.addPlayer(e.getPlayer());
	}
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		man.removePlayer(e.getPlayer());
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		man.eliminate(e.getEntity());
	}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player)) return;
		Player attacker = (Player) e.getDamager();
		if(!attacker.hasPermission("ajelimination.attack")) {
			e.setCancelled(true);
		}
	}
}
