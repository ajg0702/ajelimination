package us.ajg0702.elimination.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class VersionSupport {
	
	public static String getVersion() {
		return Bukkit.getVersion().split("\\(MC: ")[1].split("\\)")[0];
	}
	public static int getMinorVersion() {
		return Integer.parseInt(getVersion().split("\\.")[1]);
	}

}
